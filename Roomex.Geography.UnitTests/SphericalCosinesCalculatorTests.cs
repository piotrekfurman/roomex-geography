﻿using FluentAssertions;
using Rommex.Geography.Application.Geography.Distance;
using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Roomex.Geography.UnitTests;

public class SphericalCosinesCalculatorTests
{
    private readonly SphericalCosinesCalculator _sut = new();
    private static double DistanceToleranceInKm = 1.0;

    [Theory]
    [InlineData(54.516842, 18.541941, 39.904202, 116.407394, DistanceUnit.Kilometers, 6952.0)] // Gdynia - Beijing
    [InlineData(41.3851, 2.1734, 40.7128, -74.0060, DistanceUnit.Kilometers, 6166.0)] // Barcelona - New York
    [InlineData(34.0522, -118.2437, 48.8566, 2.3522, DistanceUnit.Kilometers, 9086.0)] // Los Angeles - Paris
    [InlineData(-33.8688, 151.2093, 51.5074, -0.1278, DistanceUnit.Miles, 10559)] // Sydney - London
    [InlineData(35.6895, 139.6917, 55.7558, 37.6173, DistanceUnit.Miles, 4646.0)] // Tokyo - Moscow
    [InlineData(19.0760, 72.8777, 51.5074, -0.1278, DistanceUnit.Miles, 4469.0)] // Mumbai - London
    public void CalculateDistanceInKilometers_BetweenTwoPoint_ShouldReturnApproximateDistance(
        double latitudeA, double longitudeA,
        double latitudeB, double longitudeB,
        DistanceUnit distanceUnit,
        double expectedDistance)
    {
        var coordinates = new Coordinates()
        {
            LatitudeA = latitudeA, LongitudeA = longitudeA, LatitudeB = latitudeB, LongitudeB = longitudeB
        };
        var result = _sut.CalculateDistance(coordinates, distanceUnit);

        result.Distance.Should().BeApproximately(expectedDistance, DistanceToleranceInKm);
        result.Unit.Should().Be(distanceUnit);
    }
}
