# Roomex Code Challenge

## Build
To build execute following command `dotnet build`

## Run the API

To run the API execute following command `dotnet run --project ./Roomex.Geography.Api`

## Swagger
Swagger can be accessed here: http://localhost:5127/swagger/index.html
## Test
To execute the tests execute following command `dotnet test`
