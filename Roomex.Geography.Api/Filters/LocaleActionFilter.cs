﻿using System.Globalization;
using Microsoft.AspNetCore.Mvc.Filters;
using Roomex.Geography.Contracts.Enums;

namespace Roomex.Geography.Api.Filters;

public class LocaleActionFilter : IActionFilter
{
    public void OnActionExecuting(ActionExecutingContext context)
    {
        var acceptLanguageHeader = context.HttpContext.Request.Headers["Accept-Language"].ToString();
        var preferredCulture = acceptLanguageHeader.Split(',').FirstOrDefault()?.Split(';').FirstOrDefault();

        var unit = DistanceUnit.Kilometers;
        if (!string.IsNullOrEmpty(preferredCulture))
        {
            var culture = new CultureInfo(preferredCulture);
            if (culture.TwoLetterISOLanguageName == "en" && culture.Name.EndsWith("US"))
            {
                unit = DistanceUnit.Miles;
            }
        }

        context.HttpContext.Items[Constants.UnitOfMeasurement] = unit;
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
    }
}
