﻿using FluentValidation;
using Roomex.Geography.Contracts;

namespace Roomex.Geography.Api.Validators;

public class CoordinatesValidator : AbstractValidator<Coordinates>
{
    public CoordinatesValidator()
    {
        RuleFor(x => x.LatitudeA)
            .InclusiveBetween(-90, 90);
        RuleFor(x => x.LatitudeB)
            .InclusiveBetween(-90, 90);

        RuleFor(x => x.LongitudeA)
            .InclusiveBetween(-180, 180);
        RuleFor(x => x.LongitudeB)
            .InclusiveBetween(-180, 180);
    }
}
