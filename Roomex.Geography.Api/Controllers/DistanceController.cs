using MediatR;
using Microsoft.AspNetCore.Mvc;
using Rommex.Geography.Application.Commands;
using Roomex.Geography.Api.Filters;
using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Roomex.Geography.Api.Controllers;

[ApiController]
[Route("[controller]")]
[ServiceFilter(typeof(LocaleActionFilter))]
public class DistanceController : ControllerBase
{
    private readonly IMediator _mediator;

    public DistanceController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetDistance([FromQuery] Coordinates coordinates)
    {
        var unit = (DistanceUnit)(HttpContext.Items[Constants.UnitOfMeasurement] ?? DistanceUnit.Kilometers);
        var result = await _mediator.Send(new CalculateDistanceCommand(coordinates, unit));
        return Ok(result);
    }
}
