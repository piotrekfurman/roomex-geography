﻿using MediatR;
using Rommex.Geography.Application.Geography.Distance.Abstractions;
using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Rommex.Geography.Application.Commands;

public record CalculateDistanceCommand(Coordinates Coordinates, DistanceUnit DistanceUnit) : IRequest<DistanceCalculationResult>;


public class CalculateDistanceCommandHandler : IRequestHandler<CalculateDistanceCommand, DistanceCalculationResult>
{
    private readonly IDistanceCalculator _distanceCalculator;

    public CalculateDistanceCommandHandler(IDistanceCalculator distanceCalculator)
    {
        _distanceCalculator = distanceCalculator;
    }
    public Task<DistanceCalculationResult> Handle(CalculateDistanceCommand request, CancellationToken cancellationToken)
    {
        var result = _distanceCalculator.CalculateDistance(request.Coordinates, request.DistanceUnit);
        return Task.FromResult(result);
    }
}
