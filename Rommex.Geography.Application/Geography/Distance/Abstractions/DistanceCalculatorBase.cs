﻿using Roomex.Geography.Contracts.Enums;

namespace Rommex.Geography.Application.Geography.Distance.Abstractions;

public abstract class DistanceCalculatorBase
{
    private const double EarthRadiusInKilometers = 6371.0;
    private const double EarthRadiusInMiles = 3958.8;
    protected static double DegreeToRadian(double degree) => degree * (Math.PI / 180);
    protected static double ConvertDistance(double distance, DistanceUnit unit) =>
        unit switch
        {
            DistanceUnit.Miles => distance * EarthRadiusInMiles,
            DistanceUnit.Kilometers => distance * EarthRadiusInKilometers,
            _ => distance * EarthRadiusInKilometers
        };
}
