using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Rommex.Geography.Application.Geography.Distance.Abstractions;

public interface IDistanceCalculator
{
    DistanceCalculationResult CalculateDistance(Coordinates coordinates, DistanceUnit distanceUnit);
}
