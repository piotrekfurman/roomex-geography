﻿using Rommex.Geography.Application.Geography.Distance.Abstractions;
using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Rommex.Geography.Application.Geography.Distance;

public class SphericalCosinesCalculator : DistanceCalculatorBase, IDistanceCalculator
{
    public DistanceCalculationResult CalculateDistance(Coordinates coordinates, DistanceUnit distanceUnit)
    {
        var a = DegreeToRadian(90 - coordinates.LatitudeB);
        var b = DegreeToRadian(90 - coordinates.LatitudeA);
        var  phi = DegreeToRadian(coordinates.LongitudeA - coordinates.LongitudeB);

        double cosP = Math.Cos(a) * Math.Cos(b) + Math.Sin(a) * Math.Sin(b) * Math.Cos(phi);

        cosP = Math.Min(Math.Max(cosP, -1.0), 1.0);

        var angularDistance = Math.Acos(cosP);

        var linearDistance = ConvertDistance(angularDistance, distanceUnit);

        return new DistanceCalculationResult(linearDistance, distanceUnit);
    }
}
