using System.Net;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Roomex.Geography.Contracts;
using Roomex.Geography.Contracts.Enums;

namespace Roomex.Geography.IntegrationTests;

public class DistanceControllerTests : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly HttpClient _client;
    private readonly JsonSerializerOptions _jsonSerializerOptions;

    public DistanceControllerTests(WebApplicationFactory<Program> factory)
    {
        _client = factory.CreateClient();
        _jsonSerializerOptions = new JsonSerializerOptions()
        {
            Converters = { new JsonStringEnumConverter() },
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
    }

    [Theory]
    [InlineData("en-US", DistanceUnit.Miles)]
    [InlineData("en-GB", DistanceUnit.Kilometers)]
    [InlineData("pl-PL", DistanceUnit.Kilometers)]
    public async Task GetDistance_ShouldReturnCorrectDistanceUnit_WhenLocaleIsPassed(string locale, DistanceUnit expectedUnit)
    {
        // Arrange
        _client.DefaultRequestHeaders.Add("Accept-Language", locale);

        var query = "/Distance?LatitudeA=54.516842&LongitudeA=18.541941&LatitudeB=39.904202&LongitudeB=116.407394";
        // Act
        var response = await _client.GetAsync(query);
       
        var responseContent = await response.Content.ReadFromJsonAsync<DistanceCalculationResult>(_jsonSerializerOptions);
 
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        responseContent.Unit.Should().Be(expectedUnit);
        responseContent.Distance.Should().BeGreaterThan(0);
    }
    
    [Theory]
    [InlineData(91.0, 181.0, -91.0, -180.1)] 
    [InlineData(-91.0, 0.0, 0.0, 0.0)] 
    [InlineData(0.0, 0.0, 91.0, 0.0)] 
    [InlineData(0.0, 180.12, 91.0, 0.0)] 
    public async Task GetDistance_ShouldReturnBadRequest_WhenWrongLatLongIsPassed(double latitudeA, double longitudeA, double latitudeB, double longitudeB)
    {
        // Arrange
        var query = $"/Distance?LatitudeA={latitudeA}&LongitudeA={longitudeA}&LatitudeB={latitudeB}&LongitudeB={longitudeB}";
        // Act
        var response = await _client.GetAsync($"/Distance?LatitudeA=90&LongitudeA=181&LatitudeB=50&");
        var responseString = await response.Content.ReadAsStringAsync();
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

}
