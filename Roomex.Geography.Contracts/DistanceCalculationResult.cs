﻿using Roomex.Geography.Contracts.Enums;

namespace Roomex.Geography.Contracts;

public record DistanceCalculationResult(double Distance, DistanceUnit Unit);
