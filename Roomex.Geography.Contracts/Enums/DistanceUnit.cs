﻿namespace Roomex.Geography.Contracts.Enums;

public enum DistanceUnit
{
    Kilometers, 
    Miles
}
